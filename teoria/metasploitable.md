# Metasploitable 3

És una màquina virtual preconfigurada que compta amb una sèrie de configuracions i vulnerabilitats pensades per permetre'ns depurar les nostres tècniques de hacking utilitzant 'exploits' com, per exemple, Metasploit. Aquestes vulnerabilitats poden ser contrasenyes dèbils, ports oberts, etc. En veurem diferents exemples.

Existeixen 3 versions de Metasploitable. La primera d'elles data de fa 7 anys, la segona de fa 6 anys i aquesta útlima, la versió més recent, que és la que utilitzaré en aquest projecte ja que es tracta de l'opció més recomanable a l'ésser la més actualitzada i útil per a provar les nostres habilitats avui dia.

A diferència de les versions anteriors (que eren snapshots de les màquines virtuals), aquesta nova màquina depèn de Vagrant i Packer per poder compilar fàcilment la imatge en el sistema i ser molt més dinàmica.