# Conceptes bàsics de Metasploit

+ Vulnerabilitat: punt dèbil en un sistema, ja sigui per configuració o errors en la codificació.
+ Exploit: tros de codi especialment dissenyat per aprofitar una vulnerabilitat específica en un sistema.
+ Payload: codi que s'executa després de que l'explotació de la vulnerabilitat sigui exitosa. També es 		coneix com càrrega útil.
+ Post-Explotació: procés que es du a terme després d'obtenir l'accés a un sistema mitjançant l'explotació d'una 		vulnerabilitat.
