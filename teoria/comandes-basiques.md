# Comandes bàsiques de MSF

- msfupdate: actualitzar Metasploit a la seva última versió.
- msfvenom: crear BackDoors (encoders, payloads, etc.).
- msfconsole: ingressar a la consola de Metasploit

## Consola de MSF:

	> help: ajuda de Metasploit
	> search: buscar dins del framework el que necessites (mòduls, etc.)
	> use: seleccionar i carregar qualsevol mòdul
	> info: mostrar documentació del mòdul
	> show: mostrar opcions, objectius, paràmetres, etc. del mòdul
	> set: assignar valors a les diferents opcions del mòdul
	> check: verificar el correcte funcionament de l'exploit sense executar-lo
	> run/exploit: executar el mòdul amb els paràmetres definits

![](../imatges/captura-metasploit.png)