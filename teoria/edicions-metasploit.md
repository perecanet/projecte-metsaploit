# Edicions de Metasploit

Hi ha diverses interfícies per Metasploit disponibles. Les més populars són mantingudes per Rapid7 i Estratègic Ciber LLC.

## Edició Metasploit Framework (MSF)

La versió gratuïta. Conté una interfície de línia de comandes, la importació de tercers i l'explotació manual.

## Edició Metasploit Community

L'octubre de 2011, Rapid7 va alliberar Metasploit Community Edition, una interfície d'usuari gratuïta basada a la web per a Metasploit. Aquesta inclou detecció de xarxes, navegació per mòdul i l'explotació manual.

## Edició Metasploit Express

L'abril de 2010, Rapid7 alliberà Metasploit Express, una edició comercial de codi obert, per als equips de seguretat que necessiten verificar vulnerabilitats. Ofereix una interfície gràfica d'usuari, integra *Nmap* per al descobriment, i afegeix recopilació de proves de forma automàtica.

## Edició Metasploit Pro

L'octubre de 2010, Rapid7 va afegir Metasploit Pro, que s'utilitza per a proves de penetració. Aquest inclou totes les característiques de Metasploit Express i afegeix l'exploració i explotació d'aplicacions web.

## Armitage

És una eina de gestió gràfica per ciberatacs del Projecte Metasploit, la qual visualitza objectius i recomana mètodes d'atac. És una eina per a enginyers en seguretat web i és de codi obert. Destaca per les seves contribucions a la col·laboració del *Red team*, permetent sessions compartides, dades i comunicació a través d'una única instància Metasploit.
