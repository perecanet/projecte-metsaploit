# Arquitectura de Metasploit:

+ Eines (Tools): són scripts que ajuden a l'elaboració de mòduls de Metasploit.

+ Plugins: són programes externs que fan servir recursos de Metasploit. Per exemple, aquests són els que ens permeten connectar-nos amb OpenVAS, SQLmap, Nmap, etc. a MSF Base.

+ Interfícies: és des d'on es pot fer servir Metasploit, cal aclarir que la web és només per a la versió de pagament.

+ Llibreries:

    + MSF BASE: és on hi ha la configuració i els recursos de Metasploit. Utilitza recursos de MSF Core.

    + MSF Core: ens proporciona una API bàsica amb rutines per desenvolupar programes i eines.

    + REX: és una llibreria que barreja funcions per al maneig de sockets, components adicionals del sistema, protocols, etc. Aquesta s'usa per a la majoria de les tasques.

+ Mòduls:

    + Exploits: són fragments de programari que tenen la finalitat d'aprofitar una vulnerabilitat en un sistema.
    
    + Nops: són utilitzats pels payloads perquè es puguin executar de manera satisfactòria en la memòria, evitant que el processador interrompi la càrrega d'aquests.

    + Encoders: aquests serveixen per esquivar els antivirus, codificant els payloads per no ser detectats.

    + Auxiliarys: aquests no són comunament usats pels exploits, estan més orientats a navegadors web. La majoria d'ells són utilitzats per recopilació d'informació, com hacking amb cercadors.

    + Payloads: també anomenats "càrrega útil", és la part de malware que realitza una acció maliciosa un cop l'exploit ha aprofitat la vulnerabilitat. En trobem diferents tipus:

        - **Shell de comandes:** permet als usuaris executar scripts de cobrament o executar comandes arbitràries.
        - **Meterpreter:** permet als usuaris controlar la pantalla d'un dispositiu mitjançant VNC i navegar, carregar i descarregar arxius.
        - **Càrregues dinàmiques:** permet als usuaris evadir les defenses antivirus mitjançant la generació de càrregues úniques.

## Sistema d'arxius:

+ .data: arxius editables usats per Metasploit. Es pot accedir a n'aquests en la següent ruta (Kali linux):

	    cd /usr/share/metasploit-framework/data

+ .documentation: documentació del framework.

	    cd /usr/share/metasploit-framework/documentation

+ .lib: codi base del framework.

	    cd /usr/share/metasploit-framework/lib

+ .modules: és on s'emmagatzemen els mòduls instal·lats.

	    cd /usr/share/metasploit-framework/modules

+ .tools: utilitats de la línea de comandes.

	    cd /usr/share/metasploit-framework/tools

+ .scripts: tal i com el nom ens indica, és on s'emmagatzemen els scripts.

	    cd /usr/share/metasploit-framework/scripts

+ .plugins: ens permet afegir scripts.

        cd /usr/share/metasploit-framework/plugins