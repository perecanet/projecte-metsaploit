# Metasploit

Es tracta d'un projecte de codi obert de seguretat informàtica. Aquest ens proporciona informació sobre vulnerabilitats de seguretat, ajuda en tests de penetració, també coneguts com  "Pentesting"; i el desenvolupament de signatures per a sistemes de detecció d'intrusos.

El seu subprojecte més conegut és el Metasploit Framework, eina desenvolupada en Perl i Ruby en la seva major part, que està enfocada a auditors de seguretat i equips Red Team i Blue Team.

Red Team és l'equip ofensiu o encarregat del 'hacking' ètic, que fa proves d'intrusió, mentre que el Blue Team és l'equip que porta a terme la securització i tota la part defensiva.

## Metasploit Framework

És una eina molt completa que té moltíssims 'exploits', que són  vulnerabilitats conegudes, en les quals tenen també uns mòduls, anomenats payloads, que són els codis que exploten aquestes vulnerabilitats.

També disposa d'altres tipus de mòduls, per exemple els encoders, que són una espècie de codis de xifrat per evasió d'antivirus o sistemes de seguretat perimetral.

Un altre dels avantatges d'aquest framework és que ens permet interactuar també amb eines externes, com Nmap o Nessus. A més ofereix la possibilitat d'exportar el nostre malware a qualsevol format, ja sigui en sistemes Unix o Windows.

Per últim, destacar també que és multiplataforma i, òbviament, gratuït, tot i que té una versió de pagament, en la que se'ns ofereixen 'exploits' ja desenvolupats, però el cost és força elevat. La versió gratuïta és molt interessant perquè conté totes les vulnerabilitats públiques.