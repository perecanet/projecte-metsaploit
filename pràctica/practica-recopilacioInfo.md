# Recopilació d'informació

Un cop feta la configuració de les màquines virtuals de Kali Linux i Metasploitable3 (servidor de Windows), podem començar a utilitzar Metasploit.

![](../imatges/captura-metasploit.png)

En primer lloc, durem a terme una recopilació d'informació del sistema al qual volem atacar. Així doncs, començarem executant el mòdul auxiliar **arp-sweep**, la funció del qual és descobrir els hosts de la xarxa local mitjançant peticions ARP.

## Ús del mòdul **arp-sweep**

+ Seleccionem el mòdul:
> use auxiliary/scanner/discovery/arp_sweep

+ Assignem els paràmetres necessaris, en aquest cas només cal especificar la xarxa:
> set rhosts 10.0.2.0/24

+ Executar:
> run

![](../imatges/arp-sweep.png)

Com es pot veure a la imatge, aquest ha detectat diferents hosts a la nostra xarxa, però quin d'ells es tracta del Windows de Metasploitable3? Anem a comprovar-ho.

En aquest cas he escollit el mòdul **udp-sweep**, el qual detecta serveis UDP.

## Ús del mòdul **udp-sweep**

+ Seleccionem el mòdul:
> use auxiliary/scanner/discovery/udp_sweep

+ Comprovem quins paràmetres cal introduir:
> show options

![](../imatges/udp-sweep1.png)

En aquest cas només hem de passar-li les adreces per les quals volem buscar serveis UDP, però es podria donar la situació en que haguéssim de fer-ho per moltes IPs, pel que la manera amb que ens estalviem més temps és la següent:

> hosts -R

D'aquesta forma s'assignen tots els hosts descoberts anteriorment al paràmetre *rhosts* del mòdul, com es pot observar en la següent imatge:

![](../imatges/udp-sweep2.png)

+ Un cop introduïts els camps necessaris, el mòdul està llest per executar-se:
> run

![](../imatges/udp-sweep3.png)

Quan aquest estigui llest, ja es pot veure tot el que ha trobat executant les següents comandes:
> hosts

> services

![](../imatges/udp-sweep4.png)

Què n'hem tret d'això? Doncs ara trobem diversos camps dels hosts omplerts i que abans estaven en blanc. El que més crida l'atenció és el de *os_name*, el qual indica el sistema operatiu del host en qüestió. En el cas del Metasploitable3, detecta que està corrent un Windows, com es pot veure a l'anterior captura de pantalla.

D'altra banda, aquest mòdul també ens ha proporcionat un parell de serveis que estan actius als hosts examinats: el primer d'ells es tracta del dns del *gateway* de la xarxa; i el segon del *SNMP* del Windows, que a més ens proporciona informació tant del *hardware* com del *software* d'aquest.

