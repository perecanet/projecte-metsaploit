# Instal·lació de Metasploit

1. Accedim a la [web oficial de Metasploit](https://www.metasploit.com/) i executem les comandes segons el nostre sistema operatiu, en aquest cas Fedora 30.

> ![](../imatges/1install-metasploit.png)

> ![](../imatges/2install-metasploit.png)

2. Executem *msfupdate* per assegurar-nos que està actualitzat.

> ![](../imatges/3install-metasploit.png)

3. Executem *msfconsole* per inicialitzar Metasploit i dur a terme la configuració inicial (creació de la base de dades, etc.).

> ![](../imatges/4install-metasploit.png)

4. Llest! Instal·lació acabada.

> ![](../imatges/5install-metasploit.png)
