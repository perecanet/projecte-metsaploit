# Configuració de les màquines virtuals

> ![](../imatges/oracle-vm.png)

Un cop hem acabat amb les instal·lacions de totes les eines necessàries pel projecte, anem amb la configuració de les màquines virtuals per tal de que es puguin comunicar entre elles:

+ En primer lloc, creem una xarxa NAT, anomenada Metasploit, a la qual s'hi connectarà tant el Kali Linux com el Windows del Metasploitable3.

> ![](../imatges/xarxa-nat.png)

+ Un cop creada, accedim als paràmetres d'ambdues màquines virtuals i, a l'apartat de xarxa, escollim la NAT Metasploit.

> ![](../imatges/xarxa-maquina.png)

+ Fet això, les podem arrencar i comprovar que se'ls hagi assignat una adreça IP correcta i que es puguin comunicar entre elles.

    + Kali Linux:

    > ![IP assignada: 10.0.2.15](../imatges/ip-kali.png)

    + Windows + Ping entre màquines:

    > ![IP assignada: 10.0.2.4](../imatges/ping-ip-win2008.png)

Així doncs, ja ho tenim tot llest per començar amb les pràctiques de Metasploit!

